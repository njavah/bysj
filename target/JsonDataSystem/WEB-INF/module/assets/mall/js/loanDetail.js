//进入页面请求数据
$(document).ready(function () {
    //请求基础数据
    var id = $('#loanProductId').val();
    if(id != "") {
        $.ajax({
            type: "post",
            url: "/spica/mall/loanInfo",
            dataType: "json",
            data: {"loanProductId": id},
            success: function (data) {
                var loanData = data.data.loanProductData;

                //直接显示在input中的数据
                $('#productName').val(loanData.productName);

                $('#productIcon').val(loanData.productIcon);

                $('#productIconImage').val(loanData.productIconImage);
                $('#productDesc').val(loanData.productDesc);
                $('#loanRate').val(loanData.loanRate);
                $('#applyUrl').val(loanData.applyUrl);

                $('#applyCondition').val(loanData.applyCondition);
                $('#costDesc').val(loanData.costDesc);
                $('#repaymentDesc0').val(loanData.repaymentDesc[0]);
                $('#repaymentDesc1').val(loanData.repaymentDesc[1]);
                $('#repaymentDesc2').val(loanData.repaymentDesc[2]);

                //单选
                $('#imageColor').val(loanData.imageColor).trigger('change');
                $('#loanRateType').val(loanData.loanRateType).trigger('change');
                $('#status').val(loanData.status).trigger('change');
                $('#needRealName').val(loanData.needRealName).trigger('change');
                $('#postUserInfo').val(loanData.postUserInfo).trigger('change');
                $('#profitType').val(loanData.profitType).trigger('change');

                //多选
                if(loanData.applyMaterials != null) {
                    var applyMaterials = loanData.applyMaterials.split(",");
                    $('#applyMaterials').val(applyMaterials).trigger("change");
                }
                if(loanData.productTags != null) {
                    var productTags = loanData.productTags.split(",");
                    $('#productTags').val(productTags).trigger("change");
                }
                if(loanData.productDescType != null) {
                    var productDescType = loanData.productDescType.split(",");
                    $('#productDescType').val(productDescType).trigger("change");
                }

                //简单数据处理
                if(loanData.loanPeriod != null) {
                    var loanPeriod = loanData.loanPeriod.split(",");
                    if(loanPeriod[0] != null) {
                        $('#loanPeriod0').val(loanPeriod[0]);
                    }
                    if(loanPeriod[1] != null) {
                        $('#loanPeriod1').val(loanPeriod[1]);
                    }
                }
                if(loanData.age != null) {
                    var age = loanData.age.split(",");
                    if(age[0] != null) {
                        $('#age0').val(age[0]);
                    }
                    if(age[1] != null) {
                        $('#age1').val(age[1]);
                    }
                }
                if(loanData.creditPoint != null) {
                    var creditPoint = loanData.creditPoint.split(",");
                    if(creditPoint[0] != null) {
                        $('#creditPoint0').val(creditPoint[0]);
                    }
                    if(creditPoint[1] != null) {
                        $('#creditPoint1').val(creditPoint[1]);
                    }
                }

                //复杂数据处理
                if(loanData.loanLimit != null) {
                    var loanLimit = loanData.loanLimit.split(",");
                    if(loanLimit[0] != null) {
                        if(loanLimit[0] / 10000 > 0 && loanLimit[0] % 10000 == 0) {
                            var data = loanLimit[0] / 10000 + "w";
                            $('#loanLimit0').val(data);
                        } else {
                            $('#loanLimit0').val(loanLimit[0]);
                        }

                    }
                    if(loanLimit[1] != null) {
                        if(loanLimit[1] / 10000 > 0 && loanLimit[1] % 10000 == 0) {
                            var data = loanLimit[1] / 10000 + "w";
                            $('#loanLimit1').val(data);
                        } else {
                            $('#loanLimit1').val(loanLimit[1]);
                        }
                    }
                }
            }
        });
    }
    //select2初始化
    $('#imageColor').select2();
    $('#loanRateType').select2();
    $('#status').select2();
    $('#needRealName').select2();
    $('#postUserInfo').select2();
    $('#profitType').select2();
    $('#applyMaterials').select2();
    $('#productTags').select2();
    $('#productDescType').select2();
    //动态改变select2样式
    $('.select2-container').css('width','200px');
});

//点击修改时向后台发送ajax请求
$('#modifyDetailBtn').click(function () {

    //处理多选数据
    var applyMaterials = $('#applyMaterials').val();
    var applyMaterialsStr = "";
    if (applyMaterials != null) {
        for (var i = 0; i < applyMaterials.length; i++) {
            if (i != applyMaterials.length - 1) {
                applyMaterialsStr = applyMaterialsStr + applyMaterials[i] + ",";
            } else {
                applyMaterialsStr = applyMaterialsStr + applyMaterials[i]
            }
        }
    }
    var productTags = $('#productTags').val();
    var productTagsStr = "";
    if (productTags != null) {
        for (var i = 0; i < productTags.length; i++) {
            if (i != productTags.length - 1) {
                productTagsStr = productTagsStr + productTags[i] + ",";
            } else {
                productTagsStr = productTagsStr + productTags[i]
            }
        }
    }
    var productDescType = $('#productDescType').val();
    var productDescTypeStr = "";
    if (productDescType != null) {
        for (var i = 0; i < productDescType.length; i++) {
            if (i != productDescType.length - 1) {
                productDescTypeStr = productDescTypeStr + productDescType[i] + ",";
            } else {
                productDescTypeStr = productDescTypeStr + productDescType[i]
            }
        }
    }

    //处理简单数据
    var loanPeriod = $('#loanPeriod0').val() + "," + $('#loanPeriod1').val();
    var age = $('#age0').val() + "," + $('#age1').val();
    var creditPoint = $('#creditPoint0').val() + "," + $('#creditPoint1').val();

    //复杂数据处理
    var loanLimit0 = $('#loanLimit0').val();
    var loanLimit1 = $('#loanLimit1').val();
    var loanLimit = loanLimit0.replace('w', '0000') + "," + loanLimit1.replace('w', '0000');

    var repaymentDesc = "\"" + $('#repaymentDesc0').val() + "\",\"" + $('#repaymentDesc1').val() + "\",\"" + $('#repaymentDesc2').val() + "\"";
    var applyCondition = "\"" + $('#applyCondition').val() + "\"";
    var costDesc = "\"" + $('#costDesc').val() + "\"";

    $.ajax({
        type: "post",
        url: "/spica/mall/loanModify",
        dataType: "json",
        data: {
            //基本数据
            "id": $('#loanProductId').val(),
            "productName": $('#productName').val(),

            "productIcon": $('#productIcon').val(),

            "productIconImage": $('#productIconImage').val(),
            "productDesc": $('#productDesc').val(),
            "loanRate": $('#loanRate').val(),
            "applyUrl": $('#applyUrl').val(),

            //单选
            "imageColor": $('#imageColor').val(),
            "loanRateType": $('#loanRateType').val(),
            "status": $('#status').val(),
            "needRealName": $('#needRealName').val(),
            "postUserInfo": $('#postUserInfo').val(),
            "profitType": $('#profitType').val(),

            //多选
            "applyMaterials": applyMaterialsStr,
            "productTags": productTagsStr,
            "productDescType": productDescTypeStr,

            //数据处理
            "loanPeriod": loanPeriod,
            "age": age,
            "creditPoint": creditPoint,
            "loanLimit": loanLimit,

            //描述说明数据
            'applyCondition': applyCondition,
            'costDesc': costDesc,
            'repaymentDesc': repaymentDesc

        },
        success: function (data) {
            if (data.code == 100) {
                $('#modifyDetailBtn').attr("disabled", "disabled");
                alert("修改成功");
            } else {
                alert("请重新填写空值的选项");
            }
        }
    });

});

//商品列表初始化
$(document).ready(function() {
    $('#loanMallInfo').DataTable({
        "scrollX" : "90%",
        "scrollY": "90%"
    });
});

//根据商品id查询商品
$('#searchByIdSubmit').on('click', function () {
    var id = $('#loanProductId').val();
    if((/^(\+|-)?\d+$/.test(id)) && id > 0){
        //拼接跳转
        var url = "/spica/mall/loanSearchById?loanProductId=" + id;
        window.location.href = url;
    } else if(id == "") {
        //若为空则刷新页面
        var url = "/spica/mall/loanIndex";
        window.location.href = url;
    } else {
        alert(" 请输入正确的商品id!")
    }
});

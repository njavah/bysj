//进入页面请求数据
$(document).ready(function () {
    //请求基础数据
    var id = $('#id').val();
    if(id != "") {
        $.ajax({
            type: "post",
            url: "/spica/mall/bankInfo",
            dataType: "json",
            data: {"id": id},
            success: function (data) {
                var bankData = data.data.bankInfo;

                //直接显示在input中的数据
                $('#bankDesc').val(bankData.bankDesc);
                $('#bankLogoUrl').val(bankData.bankLogoUrl);
                $('#bankCardLink').val(bankData.bankCardLink);
                $('#bankCardIds').val(bankData.bankCardIds);
                $('#cornerMark').val(bankData.cornerMark);
                $('#bankTipsArray').val(bankData.bankTipsArray);

                //单选
                $('#bankName').val(bankData.bankId).trigger('change');
                $('#status').val(bankData.status).trigger('change');
                $('#breath').val(bankData.breath).trigger('change');
            }
        });
    }
    if(id != "") {
        $('#bankName').attr("disabled", "disabled");
    }
    //select2初始化
    $('#bankName').select2();
    $('#status').select2();
    $('#breath').select2();
    //动态改变select2样式
    $('.select2-container').css('width','200px');
});

//点击修改时向后台发送ajax请求
$('#modifyDetailBtn').click(function () {

    $.ajax({
        type: "post",
        url: "/spica/mall/bankModify",
        dataType: "json",
        data: {

            "id": $('#id').val(),
            "bankId": $('#bankName').val(),
            "bankName": $('#bankName').find('option:selected').text(),
            "bankLogoUrl": $('#bankLogoUrl').val(),
            "bankDesc": $('#bankDesc').val(),
            "bankCardLink": $('#bankCardLink').val(),
            "bankCardIds": $('#bankCardIds').val(),
            "cornerMark": $('#cornerMark').val(),
            "bankTipsArray": $('#bankTipsArray').val(),
            "status": $('#status').val(),
            "breath": $('#breath').val()

        },
        success: function (data) {
            if (data.code == 100) {
                $('#bankName').attr("disabled", "disabled");
                $('#modifyDetailBtn').attr("disabled", "disabled");
                alert("修改成功");
            } else {
                alert("请重新填写空值的选项");
            }
        }
    });

});

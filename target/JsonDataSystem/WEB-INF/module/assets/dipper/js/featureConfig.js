function updateFeatureConfig(fieldName, status) {
    $.ajax({
        type: "post",
        url: "/spica/feature/updateFeatureConfig",
        dataType: "json",
        traditional: true,
        data: {
            "fieldName":fieldName,
            "status":status
        },
        success: function (data) {
            if (data.code == 1001) {
                alert("同步成功");
            } else {
                alert("同步失败");
            }
        }
    });
}

function addFeatureField() {
    $.ajax({
        type: "post",
        url: "/spica/feature/addFeatureConfig",
        dataType: "json",
        traditional: true,
        data: {
            "modelCode":$('input[name="modelCode"]').val(),
            "fieldName":$('input[name="fieldName"]').val(),
            "fieldMeaning":$('input[name="fieldMeaning"]').val(),
            "groupId":$('input[name="groupId"]').val(),
            "groupName":$('input[name="groupName"]').val(),
            "groupDescription":$('input[name="groupDescription"]').val(),
        },
        success: function (data) {
            if (data.code == 1001) {
                alert("修改成功");
            } else {
                alert("请重新填写空值的选项");
            }
        }
    });

    $('#addFeatureConfig').removeClass("show");
}

function syncData2KV(fieldName) {
    $.ajax({
        type: "post",
        url: "/spica/feature/syncData2KV",
        dataType: "json",
        traditional: true,
        data: {
            "fieldName":fieldName
        },
        success: function (data) {
            if (data.code == 1001) {
                alert("同步成功");
            } else {
                alert("同步失败");
            }
        }
    });
}
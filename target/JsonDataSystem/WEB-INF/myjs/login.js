$(function () {
    //登陆
    $("#submit").on("click",function () {
        var username = $("#username").val();
        var password = $("#password").val();
        if(username == ""){
            alert("请输入用户名");
        }else if(password == ""){
            alert("请输入密码");
        }
        var parameter = {
            userName:username,
            password:password
        }
        $.ajax({
            url:"/jsondata/login",
            type:"post",
            dataType:"json",
            data:{parameter:JSON.stringify(parameter)},
            success:function(data){
                if(data.code == 1011){
                    window.location.href = data.url;
                }else{
                    alert(data.desc);
                }
            },
            error:function () {
                alert("登陆时发生系统错误");
            }
        })

    })

    //注册
    $("#register").on("click",function () {
        var username = $("#username").val();
        var password = $("#password").val();
        if(username == ""){
            alert("请输入用户名");
        }else if(password == ""){
            alert("请输入密码");
        }
        var parameter = {
            userName:username,
            password:password
        };
        $.ajax({
            url:"/jsondata/register",
            type:"post",
            dataType:"json",
            data:{parameter:JSON.stringify(parameter)},
            success:function(data){
                if(data.code == 1011){
                    alert("注册成功");
                }else{
                    alert(data.desc);
                }
            },
            error:function () {
                alert("登陆时发生系统错误");
            }
        })

    })
})
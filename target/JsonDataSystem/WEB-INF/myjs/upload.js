var fail = new Array();
var success = new Array();

$("#reportFile").fileinput({
    language: "zh", // 未生效,
    // showPreview: false,
    // hideThumbnailContent: true,
    uploadUrl: '/jsondata/uploadData',
    uploadAsync: true,
    multiple: true,
    // hideThumbnailContent: true,
    showUploadedThumbs: false,
    maxFileCount: 10,
    autoReplace: true,
    showUpload: true,
    showRemove: true,
    allowedFileExtensions: ["json"]
}).on("fileuploaded",function (event,data,previewId,index,uploadedId) {
    if(data.response.code == 1011){
        //成功的
        //$('#'+previewId).remove();
        success.push(previewId);
    }else{
        //上传失败的
        fail.push({
            previewId:previewId,
            fileName:data.response.fileName,
            desc:data.response.desc
        });
         //$('#'+previewId).children("div").children("div").eq(1).children("div").eq(1).children("i").attr("class","glyphicon glyphicon-exclamation-sign text-danger"")
         $('#'+previewId).children("div").children("div").eq(1).html(
         " <div class='file-actions'>"+
               "<div class='file-footer-buttons'>"+
             "<button type='button' class='kv-file-upload btn btn-xs btn-default' title='Upload file'>   <i class='glyphicon glyphicon-upload text-info'></i>"+
"</button>"+
"<button type='button' class='kv-file-remove btn btn-xs btn-default' title='Remove file'><i class='glyphicon glyphicon-trash text-danger'></i></button>"+
    "</div>"+
    "<div class='file-upload-indicator' tabindex='-1' title='Upload Error'><i class='glyphicon glyphicon-exclamation-sign text-danger'></i></div>"+
    "<div class='clearfix'></div></div>");
    }

    if(success.length+fail.length == data.files.length && fail.length > 0){
        var message = "";
        for(var i = 0;i<fail.length;i++){
            message = message + fail[i].fileName+"上传失败："+fail[i].desc+"\n";
        }
        alert(message)
    }

}).on('fileerror', function(event, data, msg) {

});

$(function () {

    $("#dataplatform").addClass("active");
    $("#navBar_Upload_Information").addClass("active");
    $("#navBar_Upload_Information").css({"color":"wheat"});


    // alert( $("#mygroup").children("span").children("div").eq(2).children("div").eq(0).html())
    $("#mygroup").children("span").children("div").eq(2).children("div").eq(0).css("height","44px")
})
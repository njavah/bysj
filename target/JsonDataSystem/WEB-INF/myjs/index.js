$(function () {

    $("#dataplatform").addClass("active");
    $("#navBar_Query_Information").addClass("active");
    $("#navBar_Query_Information").css({"color":"wheat"});


    $("#form-submit").on("click",function () {
        var fileName = $("#fileName").val();
        var startTime = $("#startTime").val();
        var endTime = $("#endTime").val();
        var batchNum = $("#batchNum").val();
        var parmeter = {
            fileName:fileName,
            startTime:startTime,
            endTime:endTime,
            batchNum:batchNum,
        }
        $("#resultTable").dataTable({
            "autoWidth": true,
            "sScrollX":true,
            "ajax": {
                "url": "/jsondata/listData",
                "data":{parmeter:JSON.stringify(parmeter)},
                "dataSrc": function (data) {
                    console.log(data);
                    if(data.code == 1011 && data.data){
                        return data.data;
                    }else{
                        return new Array();
                    }
                },
            },
            "deferRender": true,
            "lengthMenu": [15, 25, 50, 75, 100],
            //允许重建
            "destroy": true,
            "columns": [
                {
                    "data": "jsonFileName",
                    "title":"JsonFileName"
                },
                {
                    "data": "productName",
                    "title":"ProductName"
                },
                {
                    "data": "productDate",
                    "title": "ProductDate",
                    "render":function (data) {
                        return new Date(data).toLocaleString().split(" ")[0];
                    }
                },
                {
                    "data": "harmonizationEquipment",
                    "title": "HarmonizationEquipment"
                },
                {
                    "data": "batchNumber",
                    "title": "BatchNumber"
                },
                {
                    "data": "productionQuantity",
                    "title": "ProductionQuantity"
                },
                {
                    "data":"processNumber",
                    "title":"ProcessNumber"
                },
                {
                    "data":"productionTemperature",
                    "title":"ProductionTemperature"
                },
                {
                    "data":"samplingTime",
                    "title":"SamplingTime"
                },
                {
                    "data":"samplingConclusion",
                    "title":"SamplingConclusion"
                },
                {
                    "data":"packingForm",
                    "title":"PackingForm"
                },
                {
                    "data":"quantityFilling",
                    "title":"QuantityFilling"
                },
                {
                    "data":"finalWeight",
                    "title":"FinalWeight"
                },
                {
                    "data":"fillingLineNumber",
                    "title":"FinaFillingLineNumberlWeight"
                },
                {
                    "data":"mainExercise",
                    "title":"MainExercise"
                },
                {
                    "data":"toReview",
                    "title":"ToReview"
                },
                {
                    "data":"reportTime",
                    "title":"ReportTime",
                    "render":function (data) {
                        return new Date(data).toLocaleString();
                    }
                },
                {
                    "data":"",
                    "title":"详情",
                    "render":function (data, type, row, meta) {
                        var url = "/jsondata/indexDetail?fileName="+row.jsonFileName+"&batchNum="+row.batchNumber+"&time="+row.productDate;
                        var urlStr = "<a href='"+url+"'>详情</a>";
                        return urlStr;
                    }
                },
                {
                    "data":"",
                    "title":"操作",
                    "render":function (data, type, row, meta) {
                        var url = "/jsondata/download?fileName="+row.jsonFileName;
                        var urlStr = "<a href='"+url+"'>下载</a>";
                        return urlStr;
                    }

                }
            ],
           /* "columnDefs": [{
                targets: [4], //第1，2，3列
                createdCell: function (td, cellData, rowData, row, col) {
                    //td:已经创建的td节点 row 行 col 列 rowdata 整行数据 cellData 单元格的数据
                    console.log(row+":"+col);
                    console.log(JSON.stringify(cellData));
                    /!*  cellData = rowData.productType1+"-"+rowData.type1Comment;
                      if(row ==0 && (col == 4 || col == 5)){
                          $(td).attr('rowspan',3);
                          //$(td).attr('colspan',1);
                      }else if(row == 4 && (col == 4 || col == 5)){
                          $(td).attr('rowspan',3);
                          //$(td).attr('colspan',1);
                      }else {
                          $(td).remove();
                      }*!/

                }
            }],*/
            //设置排序
            "order": [[1, 'asc']]
        })
    })
})


Date.prototype.toLocaleString = function() {
    var year = this.getFullYear();
    var month = this.getMonth()+1;
    month = month<10?'0'+month:month;
    var day = this.getDate();
    day = day<10?'0'+day:day;
    var hours = this.getHours();
    hours = hours<10?'0'+hours:hours;
    var min = this.getMinutes();
    min = min<10?'0'+min:min;
    var ss = this.getSeconds();
    ss = ss<10?'0'+ss:ss;
    return year+'-'+month+'-'+day+"   "+hours+":"+min+":"+ss;
    /*return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate() + "- " + this.getHours() + ":" + this.getMinutes() + ":" + this.getSeconds() ;*/
};
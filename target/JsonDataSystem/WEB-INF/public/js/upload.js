$("#reportFile").fileinput({
    language: "zh", // 未生效,
    // showPreview: false,
    // hideThumbnailContent: true,
    uploadUrl: "#",
    uploadAsync: false,
    multiple: false,
    // hideThumbnailContent: true,
    showUploadedThumbs: false,
    maxFileCount: 10,
    autoReplace: true,
    showUpload: false,
    showRemove: false,
    /*  allowedFileExtensions: ["html"],
      previewFileIconSettings: {
          "html" : "<i class='fa fa-file-excel-o text-success'></i>"
      },*/
    initialPreviewFileType: "office"
}).on('fileuploaded', function(event, previewId, index, fileId) {
    console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
}).on('fileuploaderror', function(event, data, msg) {
    alert("发生异常")
    console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
}).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
    console.log('File Batch Uploaded', preview, config, tags, extraData);
});

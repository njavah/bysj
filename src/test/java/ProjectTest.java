
import com.mchange.v2.c3p0.ComboPooledDataSource;
import enums.AnalysisStatusEnum;
import enums.ErrorEnum;
import org.junit.Test;

import java.sql.*;

public class ProjectTest {

    @Test
    public  void connectDB() {
        //声明Connection对象
        Connection con;
        //驱动程序名
        String driver = "com.mysql.jdbc.Driver";
        //URL指向要访问的数据库名mydata
        String url = "jdbc:mysql://localhost:3306/nhdata?useUnicode=true&characterEncoding=utf8&useSSL=false";
        //MySQL配置时的用户名
        String user = "root";
        //MySQL配置时的密码
        String password = "KUQI188014.nh";
        //遍历查询结果集
        try {
            //加载驱动程序
            Class.forName(driver);
            //1.getConnection()方法，连接MySQL数据库！！
            con = DriverManager.getConnection(url,user,password);

            //con.close();
        } catch(ClassNotFoundException e) {
            //数据库驱动类异常处理
            System.out.println("Sorry,can`t find the Driver!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.out.println("fail");
            //数据库连接失败异常处理
            e.printStackTrace();
        }catch (Exception e) {
            System.out.println("fail");
            // TODO: handle exception
            e.printStackTrace();
        }finally{
            System.out.println("success");
        }
    }

    @Test
    public void connectDBByc3p0(){
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        // 设置连接数据库需要的配置信息
        try {
            cpds.setDriverClass("com.mysql.jdbc.Driver");
            cpds.setJdbcUrl("jdbc:mysql://localhost:3306/nhdata?useUnicode=true&characterEncoding=utf8&useSSL=false");
            cpds.setUser("root");
            cpds.setPassword("KUQI188014.nh");
            //2.设置连接池的参数
            cpds.setInitialPoolSize(5);
            cpds.setMaxPoolSize(15);
            System.out.println(cpds.getConnection());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void enumTest(){
        String str = ErrorEnum.getByValue(ErrorEnum.DUPLICSTEKEYERROR.getCode()).getDesc();
        System.out.println(ErrorEnum.ANALYTICALDATAERROR.getDesc());
        System.out.println(AnalysisStatusEnum.READ.getName());
        System.out.println(str);
    }
}

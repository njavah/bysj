package controller;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

public class LogTest {

    private String str = "";
    private Logger logger = LoggerFactory.getLogger(LogTest.class);

    @Test
    public void logPathTest() throws Exception{
        File directory = new File("");// 参数为空
        String courseFile = directory.getCanonicalPath();//获取当前项目的根路径
        directory = new File(courseFile);
        String parentPath = directory.getParentFile().getPath();
        File file = new File(parentPath+File.separator+"JsonDataLog");
        if(!file.exists()){
            file.mkdirs();
        }
    }

    @Test
    public void logTest() throws Exception{
       logger.error("error");
    }
}

package controller;

import Exceptions.InnerException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sun.xml.internal.ws.util.xml.ContentHandlerToXMLStreamWriter;
import dto.LoginUser;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.javassist.runtime.Inner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import utils.AnalysisDataUtils;
import dto.ControlParameter;
import dto.JsonData;
import dto.OperationProcess;
import enums.ErrorEnum;
import enums.SuccessEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import service.*;
import utils.ObjectUtils;
import utils.TimeUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Controller
@RequestMapping("/jsondata")
public class JsonDataController {

    private Logger logger = LoggerFactory.getLogger(JsonDataController.class);

    @Autowired
    private JsonDataService jsonDataService;
    @Autowired
    private ParameterService parameterService;
    @Autowired
    private ProcessService processService;
    @Autowired
    private UploadService uploadService;
    @Autowired
    private LoginUserService loginUserService;



    //基础数据展示首页
    @RequestMapping("/index")
    public String index(){
        return "jsondata/index";
    }

    //登陆首页
    @RequestMapping("/indexLogin")
    public String indexLogin(){
        return "jsondata/login";
    }

    //退出登陆
    @RequestMapping("/exitLogin")
    public void exitLogin(HttpServletRequest request,HttpServletResponse response){
        request.getSession().removeAttribute("userName");
        try {
            response.sendRedirect("/jsondata/indexLogin");
        }catch (Exception e){
            logger.error("JsonDataController exitLogin occur exception.message:{}",e.getMessage(),e);
        }
    }

    //详细信息展示首页
    @RequestMapping("/indexDetail")
    public ModelAndView idnexDetail(ModelAndView modelAndView,HttpServletRequest request){
        String fileName = request.getParameter("fileName");
        String batchNum = request.getParameter("batchNum");
        String time = request.getParameter("time");
        logger.info("JsonDataController show indexDetail init.fileName:{},batchNum:{},time:{}",fileName,batchNum,time);
        modelAndView.addObject("fileName",fileName==null?"":fileName);
        modelAndView.addObject("batchNum",batchNum==null?"":batchNum);
        try {
            modelAndView.addObject("time",time==null?"": TimeUtils.millisecondToStringLong(Long.valueOf(time)));
        }catch (Exception e){
            logger.error("JsonDataController show indexDetail occur exception.time:{},message:{}",time,e.getMessage(),e);
            modelAndView.addObject("time","");
        }

        modelAndView.setViewName("jsondata/detail");
        return modelAndView;
    }

    //上传数据首页
    @RequestMapping("/upload")
    public String upload(){
        return "jsondata/upload";
    }

    //上传数据
    @RequestMapping(value = "/uploadData",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String uploadData(@RequestParam(value = "reportFile") MultipartFile file,
                             HttpServletRequest request){

        System.out.println(file.getOriginalFilename());

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",SuccessEnum.SUCCESS.getCode());
        AnalysisDataUtils analysisDataUtils = new AnalysisDataUtils();
        JsonData jsonData = null;
        try {
            String data = analysisDataUtils.resultResult(file);
            jsonData = analysisDataUtils.returnDataAuto(data);
            uploadService.insertData(jsonData);
        }catch (InnerException e){
            logger.error("JsonDataController save jsondata occur exception.fileName:{},code:{},message:{}",file.getOriginalFilename(),e.getCode(),e.getMessage(),e);
            jsonObject.put("code",e.getCode());
            jsonObject.put("desc",ErrorEnum.getByValue(e.getCode()).getDesc());
            jsonObject.put("fileName",file.getOriginalFilename());
        }
        return JSON.toJSONString(jsonObject);
    }

    /**
     * 查询jsondata的数据
     *
     * @param parmeter
     *
     * fileName:文件名
     * batchNum:批量编号
     * startTime:起始时间
     * endTime:结束时间
     * @return
     */
    @RequestMapping(value = "/listData",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String listData(String parmeter){
        logger.info("JsonDataController listData init.parameter:{}",parmeter);
        Map<String,Object> resultMap = new HashMap<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = JSONObject.parseObject(parmeter);
        }catch (Exception e){
            logger.error("JsonDataController listData occur exception.parmeter:{},message:{}",parmeter,e.getMessage(),e);
            resultMap.put("code",ErrorEnum.ANALYTICALDATAERROR.getCode());
            resultMap.put("desc",ErrorEnum.ANALYTICALDATAERROR.getDesc());
            return JSON.toJSONString(resultMap);
        }

        Map<String,Object> map = new HashMap<>();
        if(null != jsonObject.get("fileName")){
            map.put("fileName",jsonObject.get("fileName"));
        }
        if(null != jsonObject.get("batchNum")){
            map.put("batchNum",jsonObject.get("batchNum"));
        }
        if(null != jsonObject.get("startTime")){
            map.put("startTime",jsonObject.get("startTime"));
        }
        if(null != jsonObject.get("endTime")){
            map.put("endTime",jsonObject.get("endTime"));
        }
        List<JsonData> list = null;
        try {
           list = jsonDataService.listJsonData(map);
        }catch (InnerException e){
            logger.error("JsonDataController listData occur InnerException.code:{},message:{}",e.getCode(),e.getMessage(),e);
            resultMap.put("code",e.getCode());
            resultMap.put("desc",ErrorEnum.getByValue(e.getCode()).getDesc());
            return JSON.toJSONString(resultMap);
        }
        resultMap.put("code",SuccessEnum.SUCCESS.getCode());
        resultMap.put("desc",SuccessEnum.SUCCESS.getDesc());
        resultMap.put("data",list);
        return  JSON.toJSONString(resultMap);

    }


    /**
     * 通过文件名查询其详细信息
     * @param fileName 文件名
     * @return
     */
    @RequestMapping(value = "/listDetail",produces = {"application/json;charset=UTF-8"} )
    @ResponseBody
    public String listDetail(String fileName){
        logger.info("JsonDataController listDetail init.fileName:{}",fileName);
        Map<String,Object> map = new HashMap<>();
        if(ObjectUtils.isNUll(fileName)){
            map.put("code",ErrorEnum.PARAMETERERROR.getCode());
            map.put("desc",ErrorEnum.PARAMETERERROR.getDesc());
            return JSON.toJSONString(map);
        }
        List<ControlParameter> controlParameterList = null;
        List<OperationProcess> operationProcessList = null;
        JsonData jsonData = null;
        try {
            jsonData = jsonDataService.getJsonDataByFileName(fileName);
            if(!ObjectUtils.isNUll(jsonData)){
                controlParameterList = parameterService.selectByForignId(jsonData.getControlParameterId());
                operationProcessList = processService.selectByForignId(jsonData.getOperationProcessListId());
            }
        }catch (InnerException e){
            logger.error("JsonDataController listDetail occur InnerException.jsonData:{},controlParameterList:{},operationProcessList:{},code:{},message:{}",JSON.toJSONString(jsonData),JSON.toJSONString(controlParameterList),JSON.toJSONString(operationProcessList),e.getCode(),e.getMessage(),e);
            map.put("code",e.getCode());
            map.put("desc",ErrorEnum.getByValue(e.getCode()).getDesc());
            return JSON.toJSONString(map);
        }
        map.put("controlParameterList",controlParameterList);
        map.put("operationProcessList",operationProcessList);
        map.put("code",SuccessEnum.SUCCESS.getCode());
        map.put("desc",SuccessEnum.SUCCESS.getDesc());
        return JSON.toJSONString(map);
    }

    //下载文件
    @RequestMapping(value = "/download",produces = {"application/json;charset=UTF-8"} )
    public void download(HttpServletRequest request, HttpServletResponse response){
        String fileName = request.getParameter("fileName");
        logger.info("JsonDataController downloadFile init.fileName:{}",fileName);
        List<ControlParameter> controlParameterList = null;
        List<OperationProcess> operationProcessList = null;
        JsonData jsonData = null;
        try {
            jsonData = jsonDataService.getJsonDataByFileName(fileName);
            //去掉无需参数 还原最原始的参数
            if(!ObjectUtils.isNUll(jsonData)){
                controlParameterList = parameterService.selectByForignId(jsonData.getControlParameterId());
                for(ControlParameter controlParameter:controlParameterList){
                    controlParameter.setId(null);
                    controlParameter.setForignId(null);
                }
                operationProcessList = processService.selectByForignId(jsonData.getOperationProcessListId());

                jsonData.setControlParameters((ArrayList<ControlParameter>) controlParameterList);
                jsonData.setOperationProcessList((ArrayList)operationProcessList);
                for(OperationProcess operationProcess:operationProcessList){
                    List<ControlParameter> tempList = parameterService.selectByForignId(operationProcess.getId());
                    for(ControlParameter controlParameter:tempList){
                        controlParameter.setId(null);
                        controlParameter.setForignId(null);
                    }
                    operationProcess.setFKControlParameters((ArrayList<ControlParameter>) tempList);
                    operationProcess.setId(null);
                    operationProcess.setForignId(null);
                }
            }
            jsonData.setId(null);
            jsonData.setControlParameterId(null);
            jsonData.setOperationProcessListId(null);
            String dataStr = JSON.toJSONString(jsonData);
            byte[] data = dataStr.getBytes();
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            response.addHeader("Content-Length", "" + data.length);
            response.setContentType("application/octet-stream;charset=UTF-8");
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(data);
            outputStream.flush();
            outputStream.close();
        }catch (Exception e){
            logger.error("JsonDataController downloadFile occur Exception.message:{}",e.getMessage(),e);
        }
    }

    /**
     * 通过id查询生产参数
     * @param id operationProcess的主键
     * @return
     */
    @RequestMapping(value = "/listOperationParameter",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String listOperationParameter(Long id){
        logger.info("JsonDataController listOperationParameter init.id:{}",id);
        Map<String,Object> map = new HashMap<>();
        List<ControlParameter> list = null;
        try {
            list =  parameterService.selectByForignId(id);
        }catch (InnerException e){
            logger.error("JsonDataController listOperationParameter occur InnerException.code:{},message:{}",e.getCode(),e.getMessage(),e);
            map.put("code",e.getCode());
            map.put("desc",ErrorEnum.getByValue(e.getCode()).getDesc());
            return JSON.toJSONString(map);
        }
        map.put("code",SuccessEnum.SUCCESS.getCode());
        map.put("desc",SuccessEnum.SUCCESS.getDesc());
        map.put("data",list);
        return JSON.toJSONString(map);
    }


    /**
     *
     * @param parameter  用户名+密码
     * @return
     */
    @RequestMapping(value = "/login",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String login(String parameter,HttpServletRequest request,HttpServletResponse response){
        Map<String,Object> map = new HashMap<>();
        try {
            LoginUser loginUser = JSONObject.parseObject(parameter,LoginUser.class);
            if(loginUserService.isExist(loginUser.getUserName(),loginUser.getPassword())){
                //登陆成功
                request.getSession().setAttribute("userName",loginUser.getUserName());
                map.put("code",SuccessEnum.SUCCESS.getCode());
                map.put("url","/jsondata/index");
            }
        }catch (InnerException e){
             map.put("code",e.getCode());
             map.put("desc",ErrorEnum.getByValue(e.getCode()).getDesc());
        }
        return JSON.toJSONString(map);
    }

    @RequestMapping(value = "/register",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String register(String parameter,HttpServletRequest request,HttpServletResponse response){
        Map<String,Object> map = new HashMap<>();
        try {
            LoginUser loginUser = JSONObject.parseObject(parameter,LoginUser.class);
            if(loginUserService.insert(loginUser.getUserName(),loginUser.getUserName())){
                map.put("code",SuccessEnum.SUCCESS.getCode());
            }else{
                map.put("code",ErrorEnum.OPERDBERROR.getCode());
                map.put("desc","注册失败");
            }
        }catch (InnerException e){
            map.put("code",e.getCode());
            map.put("desc",ErrorEnum.getByValue(e.getCode()).getDesc());
        }
        return JSON.toJSONString(map);
    }

}

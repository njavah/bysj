package dao;

import dto.OperationProcess;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ProcessMapper {
    int insertProcess(List<OperationProcess> processList);

    List<OperationProcess> selectByForignId(Long forignId);
}

package dao;

import dto.LoginUser;

public interface LoginUserMapper {
    LoginUser selectByExample(LoginUser loginUser);
    int insert(LoginUser loginUser);
}

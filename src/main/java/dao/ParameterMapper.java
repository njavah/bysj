package dao;

import dto.ControlParameter;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ParameterMapper {
    int insertParameter(List<ControlParameter> parameterList);

    List<ControlParameter> selectByForignId(Long foringId);
}

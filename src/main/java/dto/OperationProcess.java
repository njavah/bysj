package dto;

import java.util.ArrayList;

public class OperationProcess {
   private Long id;
   private String FKShowName;
   private String WLShowName;
   private String SetShowVal;
   private Long BeginTime;
   private Long EndTime;
   private String EndState;
   private ArrayList<ControlParameter> FKControlParameters;
   private Long forignId;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getFKShowName() {
      return FKShowName;
   }

   public void setFKShowName(String FKShowName) {
      this.FKShowName = FKShowName;
   }

   public String getWLShowName() {
      return WLShowName;
   }

   public void setWLShowName(String WLShowName) {
      this.WLShowName = WLShowName;
   }

   public String getSetShowVal() {
      return SetShowVal;
   }

   public void setSetShowVal(String setShowVal) {
      SetShowVal = setShowVal;
   }

   public Long getBeginTime() {
      return BeginTime;
   }

   public void setBeginTime(Long beginTime) {
      BeginTime = beginTime;
   }

   public Long getEndTime() {
      return EndTime;
   }

   public void setEndTime(Long endTime) {
      EndTime = endTime;
   }

   public String getEndState() {
      return EndState;
   }

   public void setEndState(String endState) {
      EndState = endState;
   }

   public ArrayList<ControlParameter> getFKControlParameters() {
      return FKControlParameters;
   }

   public void setFKControlParameters(ArrayList<ControlParameter> FKControlParameters) {
      this.FKControlParameters = FKControlParameters;
   }

   public Long getForignId() {
      return forignId;
   }

   public void setForignId(Long forignId) {
      this.forignId = forignId;
   }

   @Override
   public String toString() {
      return "OperationProcess{" +
              "id=" + id +
              ", FKShowName='" + FKShowName + '\'' +
              ", WLShowName='" + WLShowName + '\'' +
              ", SetShowVal='" + SetShowVal + '\'' +
              ", BeginTime=" + BeginTime +
              ", EndTime=" + EndTime +
              ", EndState='" + EndState + '\'' +
              ", FKControlParameters=" + FKControlParameters +
              ", forignId=" + forignId +
              '}';
   }
}

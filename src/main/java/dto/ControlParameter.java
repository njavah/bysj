package dto;

public class ControlParameter {
    private Long id;
    private String ParmShowName;
    private Float ParmVal;
    private String ParmUD;
    private Long forignId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParmShowName() {
        return ParmShowName;
    }

    public void setParmShowName(String parmShowName) {
        ParmShowName = parmShowName;
    }

    public Float getParmVal() {
        return ParmVal;
    }

    public void setParmVal(Float parmVal) {
        ParmVal = parmVal;
    }

    public String getParmUD() {
        return ParmUD;
    }

    public void setParmUD(String parmUD) {
        ParmUD = parmUD;
    }

    public Long getForignId() {
        return forignId;
    }

    public void setForignId(Long forignId) {
        this.forignId = forignId;
    }

    @Override
    public String toString() {
        return "ControlParameter{" +
                "id=" + id +
                ", ParmShowName='" + ParmShowName + '\'' +
                ", ParmVal=" + ParmVal +
                ", ParmUD='" + ParmUD + '\'' +
                ", forignId=" + forignId +
                '}';
    }
}

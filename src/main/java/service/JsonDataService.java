package service;

import Exceptions.InnerException;
import com.alibaba.fastjson.JSON;
import dao.JsonMapper;
import dto.JsonData;
import enums.ErrorEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.ObjectUtils;

import java.util.List;
import java.util.Map;

@Service
public class JsonDataService {

    private Logger logger = LoggerFactory.getLogger(JsonDataService.class);
    @Autowired
    private JsonMapper jsonMapper;

    public List<JsonData> listJsonData(Map<String,Object> map){

        List<JsonData> list = null;
        try {
            list = jsonMapper.listJsonData(map);
        }catch (Exception e){
            logger.error("JsonDataService selectByForignId occur exception.map:{},message:{}", JSON.toJSONString(map),e.getMessage(),e);
            throw new InnerException(ErrorEnum.OPERDBERROR.getCode(),e.getMessage(),e);
        }
        return list;
    }

    public JsonData getJsonDataByFileName(String fielName){
        if(ObjectUtils.isNUll(fielName)){
            throw new InnerException(ErrorEnum.PARAMETERERROR.getCode());
        }
        JsonData parameter = new JsonData();
        parameter.setJsonFileName(fielName);

        JsonData jsonData =null;
        try {
            jsonData = jsonMapper.returnJsonDataByFileName(parameter);
        }catch (Exception e){
            logger.error("JsonDataService selectByForignId occur exception.fielName:{},message:{}", fielName,e.getMessage(),e);
            throw new InnerException(ErrorEnum.OPERDBERROR.getCode(),e.getMessage(),e);
        }
        return jsonData;
    }
}

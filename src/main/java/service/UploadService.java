package service;

import Exceptions.InnerException;
import Exceptions.RollbackException;
import com.alibaba.fastjson.JSON;
import dao.JsonMapper;
import dao.ParameterMapper;
import dao.ProcessMapper;
import dto.*;
import enums.AnalysisStatusEnum;
import enums.ErrorEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import utils.AnalysisDataUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

@Service
public class UploadService extends Observable{
    private Logger logger = LoggerFactory.getLogger(UploadService.class);
    @Autowired
    private TransactionTemplate transactionTemplate;
    @Autowired
    private JsonMapper jsonMapper;
    @Autowired
    private ParameterMapper parameterMapper ;
    @Autowired
    private ProcessMapper processMapper;


    @Transactional(isolation= Isolation.DEFAULT,propagation=Propagation.REQUIRED,rollbackFor= InnerException.class)
    public void insertData(final JsonData jsonData) throws InnerException{

        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                logger.info("UploadService insertData init.data:{}",jsonData.toString());
                if(jsonData!=null){
                    try {
                        AnalysisDataUtils.status = AnalysisStatusEnum.STORE;

                        jsonMapper.insertJsonData(jsonData);
                        List<ControlParameter> parameterArrayList = jsonData.getControlParameters();
                        parameterMapper.insertParameter(parameterArrayList);
                        processMapper.insertProcess(jsonData.getOperationProcessList());
                        for(int i=0;i<jsonData.getOperationProcessList().size();i++){
                            ArrayList<ControlParameter> arrayList = jsonData.getOperationProcessList().get(i).getFKControlParameters();
                            for(int j=0;j<arrayList.size();j++){
                                arrayList.get(j).setForignId(jsonData.getOperationProcessList().get(i).getId());
                            }
                            parameterMapper.insertParameter(arrayList);
                        }
                        AnalysisDataUtils.status = AnalysisStatusEnum.STORESUCCESS;

                    }catch (DuplicateKeyException duplicateKeyException){
                        logger.error("UploadService insertData occur duplicateKeyException.jsonData:{}", JSON.toJSONString(jsonData));
                        throw new InnerException(ErrorEnum.DUPLICSTEKEYERROR.getCode(),duplicateKeyException.getMessage(),duplicateKeyException);
                    }catch (Exception e){
                        logger.error("UploadService insertData occur Exception.jsonData:{},message:{}", JSON.toJSONString(jsonData),e.getMessage(),e);
                        throw new InnerException(ErrorEnum.OPERDBERROR.getCode(),e.getMessage(),e);
                    }finally {
                        AnalysisDataUtils.status = AnalysisStatusEnum.STOREFAIL;
                    }
                }
            }
        });
    }

}

package service;

import Exceptions.InnerException;
import dao.ParameterMapper;
import dto.ControlParameter;
import enums.ErrorEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.ObjectUtils;

import java.util.List;

@Service
public class ParameterService {
    @Autowired
    private ParameterMapper parameterMapper;

    private Logger logger = LoggerFactory.getLogger(ParameterService.class);

    public List<ControlParameter> selectByForignId(Long foringId){
        if(ObjectUtils.isNUll(foringId)){
            throw new InnerException(ErrorEnum.PARAMETERERROR.getCode());
        }
        List<ControlParameter> list = null;
        try {
            list = parameterMapper.selectByForignId(foringId);
        }catch (Exception e){
            logger.error("ParameterService selectByForignId occur exception.foringId:{},message:{}",foringId,e.getMessage(),e);
            throw new InnerException(ErrorEnum.OPERDBERROR.getCode(),e.getMessage(),e);
        }
        return list;
    }
}

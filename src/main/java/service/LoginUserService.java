package service;

import Exceptions.InnerException;
import dao.LoginUserMapper;
import dto.LoginUser;
import enums.ErrorEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.ObjectUtils;

@Service
public class LoginUserService {
    private Logger logger = LoggerFactory.getLogger(LoginUserService.class);
    @Autowired
    private LoginUserMapper loginUserMapper;
    public Boolean isExist(String userName,String password){
        logger.info("LoginUserService isExist init.userName:{},password:{}",userName,password);
        //参数校验
        if(ObjectUtils.isNUll(userName) || ObjectUtils.isNUll(password)){
            throw new InnerException(ErrorEnum.PARAMETERERROR.getCode(),ErrorEnum.PARAMETERERROR.getDesc(),null);
        }
        LoginUser parameter = new LoginUser();
        parameter.setPassword(password);
        parameter.setUserName(userName);
        try {
            LoginUser loginUser = loginUserMapper.selectByExample(parameter);
            if(ObjectUtils.isNUll(loginUser)){
                //该用户不存在
                throw  new InnerException(ErrorEnum.UERNAMEERROR.getCode(),ErrorEnum.UERNAMEERROR.getDesc(),null);
            }else if(!parameter.getPassword().equals(loginUser.getPassword())){
                //密码错误
                throw  new InnerException(ErrorEnum.PASSWORDERROR.getCode(),ErrorEnum.PASSWORDERROR.getDesc(),null);
            }
        }catch (InnerException innerException){
            throw innerException;
        }catch (Exception e){
            logger.error("LoginUserService isExist occur Exception.userName:{},password:{},messsage:{}",userName,password,e.getMessage(),e);
            throw new InnerException(ErrorEnum.OPERDBERROR.getCode(),e.getMessage(),e);
        }
        return true;
    }


    public Boolean insert(String userName,String password){
        logger.info("LoginUserService insert init.userName:{},password:{}",userName,password);
        int ret = 1;
        //参数校验
        if(ObjectUtils.isNUll(userName) || ObjectUtils.isNUll(password)){
            throw new InnerException(ErrorEnum.PARAMETERERROR.getCode(),ErrorEnum.PARAMETERERROR.getDesc(),null);
        }
        LoginUser parameter = new LoginUser();
        parameter.setPassword(password);
        parameter.setUserName(userName);
        try {
            ret = loginUserMapper.insert(parameter);
        }catch (Exception e){
            logger.error("LoginUserService insert occur Exception.userName:{},password:{},messsage:{}",userName,password,e.getMessage(),e);
            throw new InnerException(ErrorEnum.OPERDBERROR.getCode(),e.getMessage(),e);
        }
        return ret==1?true:false;
    }
}

package service;

import Exceptions.InnerException;
import dao.ProcessMapper;
import dto.OperationProcess;
import enums.ErrorEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.ObjectUtils;

import java.util.List;

@Service
public class ProcessService {
    @Autowired
    private ProcessMapper processMapper;
    private Logger logger = LoggerFactory.getLogger(ProcessService.class);
    public List<OperationProcess> selectByForignId(Long forignId){
        if(ObjectUtils.isNUll(forignId)){
            throw new InnerException(ErrorEnum.PARAMETERERROR.getCode());
        }
        List<OperationProcess> list = null;
        try {
            list = processMapper.selectByForignId(forignId);
        }catch (Exception e){
            logger.error("ProcessService selectByForignId occur exception.forignId:{},message:{}",forignId,e.getMessage(),e);
            throw new InnerException(ErrorEnum.OPERDBERROR.getCode(),e.getMessage(),e);
        }
        return list;
    }
}

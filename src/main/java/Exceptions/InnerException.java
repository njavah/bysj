package Exceptions;

public class InnerException extends RuntimeException{

    private Integer code;

    public InnerException(){
        super();
    }

    public InnerException(String message){
        super(message);
    }

    public InnerException(Integer code){
        this.code = code;
    }

    public InnerException(Integer code,String message,Throwable throwable){
        super(message,throwable);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}

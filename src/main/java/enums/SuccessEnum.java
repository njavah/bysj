package enums;

public enum SuccessEnum {
    SUCCESS(1011,"成功");

    private Integer code;
    private String desc;

    private SuccessEnum(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

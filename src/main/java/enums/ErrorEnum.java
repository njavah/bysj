package enums;

import java.util.HashMap;
import java.util.Map;

public enum ErrorEnum {

    PARAMETERERROR(1030,"参数错误"),
    SYSTEMERROR(1031,"系统错误"),
    OPERDBERROR(1032,"存储数据失败"),
    FILESTREAMERROR(1033,"处理文件失败"),
    DUPLICSTEKEYERROR(1034,"该文件已存在"),
    LOGINERROR(1035,"登录时发生异常"),
    UERNAMEERROR(1036,"该用户不存在"),
    PASSWORDERROR(1037,"密码错误"),
    ANALYTICALDATAERROR(1038,"解析数据失败");


    private Integer code;
    private String desc;

    private ErrorEnum(Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private static  Map<Integer,ErrorEnum> errorEnumMap = new HashMap<>();

    static {
        for (ErrorEnum et : ErrorEnum.values()) {
            errorEnumMap.put(et.code, et);
        }
    }

    public static ErrorEnum getByValue(Integer value) {
        if (null == value) {
            return null;
        }
        return errorEnumMap.get(value);
    }
}

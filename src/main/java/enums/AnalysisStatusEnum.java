package enums;

public enum AnalysisStatusEnum {
    READ("读取文件中",1),//��ȡ�ļ�
    READSUCCESS("读取文件成功",2),//��ȡ�ɹ�
    READFAIL("读取文件失败",3),//��ȡʧ��
    ANALY("解析文件中",4),//�����ļ�
    ANALYSUCCESS("解析文件成功",5),//�����ɹ�
    ANALYFAIL("解析文件失败�",6),//����ʧ��
    STORE("存储数据中",7),
    STOREFAIL("存储数据失败",8),
    STORESUCCESS("存储数据成功",9);
    private String name;
    private int status;
    private AnalysisStatusEnum(String name, int status){
        this.name = name;
        this.status = status;
    }
    public String getName(){
        return name;
    }
}

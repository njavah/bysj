package utils;

import Exceptions.InnerException;
import Exceptions.RollbackException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import dto.*;
import enums.AnalysisStatusEnum;
import enums.ErrorEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;


import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;

/**
 * @Author:NiHai
 * @Description �������ݹ�����
 * @Date: 18:21 2019/1/23
 */
public class AnalysisDataUtils extends Observable {
    private Logger logger = LoggerFactory.getLogger(AnalysisDataUtils.class);
    public static AnalysisStatusEnum status = AnalysisStatusEnum.READ;

    /**
     *  ͨ���ļ�·�������ļ�
     * @param path �ļ�·��
     * @return �������ݶ�Ӧ��ʵ����
     */
    public AnalysisDataUtils(){

    }


    public String returnResult(String path){
        logger.info("returnData     ByPath:path=>{}",path);
        StringBuilder realData = new StringBuilder();
        String tempText;
        InputStreamReader isr;
        BufferedReader br = null;
        File file = null;
        try {
            file = new File(path);
            if(file.isFile() && file.exists()){
                status = AnalysisStatusEnum.READ;
                isr = new InputStreamReader(new FileInputStream(file), "utf-8");
                br = new BufferedReader(isr);
                while ((tempText= br.readLine()) != null) {
                    realData.append(tempText);
                }
            }else{
                logger.error("returnDataByPath:�ļ�·�� {}����",file.getPath());
                status = AnalysisStatusEnum.READFAIL;
                //ǰ�����
            }
        }catch (Exception e){
            logger.error("returnDataByPath:file=>{},Exception=>{},msg=>{}",file,e.getMessage(),e);
            status = AnalysisStatusEnum.READFAIL;
            throw new RollbackException(e.toString());
            //��ȡ�ļ�ʧ��
        }finally {
            if(br!=null){
                try{
                    br.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
        status = AnalysisStatusEnum.READSUCCESS;

        return realData.toString();
    }


    public String resultResult(MultipartFile file) throws InnerException{
        StringBuilder realData = new StringBuilder();
        try {
            String tempText;

            InputStream inputStream =  file.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while ((tempText= bufferedReader.readLine()) != null) {
                realData.append(tempText);
            }

        }catch (Exception e){
            logger.error("AnalysisDataUtils resultResult occur Exception,file:{},message:{}", JSON.toJSONString(file),e.getMessage(),e);
            throw new InnerException(ErrorEnum.FILESTREAMERROR.getCode(),e.getMessage(),e);
        }
        return realData.toString();

    }

    public JsonData returnDataByPath(String path){
        return  returnData(returnResult(path));
    }

    /**
     *  ͨ���ļ����ݽ���
     * @param data ��ʵ������
     * @return �������ݶ�Ӧ��ʵ����
     */
    public  JsonData returnData(String data){
        JsonData jsonData = new JsonData();
       /* status = AnalysisStatusEnum.ANALY;

        try {
            JSONObject jsonObject =  JSONObject.fromObject(data);

            ArrayList<ControlParameter> parameterArrayList = new ArrayList<>();
            JSONArray controlParameterArray = (JSONArray)jsonObject.get("ControlParameters");

            jsonData.setProductDate(getCalendar((String)jsonObject.get("ProductDate")).getTimeInMillis());

            for(int i=0;i<controlParameterArray.size();i++){
                ControlParameter temp = new ControlParameter();
                temp.setParmShowName((String) ((JSONObject)controlParameterArray.get(i)).get("ParmShowName"));
                temp.setParmVal((Float) ((JSONObject)controlParameterArray.get(i)).get("ParmVal"));
                temp.setParmUD((String) ((JSONObject)controlParameterArray.get(i)).get("ParmUD"));
                temp.setForignId(jsonData.getProductDate());
                parameterArrayList.add(temp);
            }
            JSONObject parameterObject = ((JSONObject)controlParameterArray.get(0));
            jsonData.setControlParameters(parameterArrayList);
            jsonData.setJsonFileName((String)jsonObject.get("JsonFileName"));
            jsonData.setProductName((String)jsonObject.get("ProductName"));
            jsonData.setHarmonizationEquipment((String)jsonObject.get("HarmonizationEquipment"));
            jsonData.setBatchNumber((String)jsonObject.get("BatchNumber"));
            jsonData.setProductionQuantity((String)jsonObject.get("ProductionQuantity"));
            jsonData.setProcessNumber((String)jsonObject.get("ProcessNumber"));
            jsonData.setProductionTemperature((String)jsonObject.get("ProductionTemperature"));
            jsonData.setSamplingTime((String)jsonObject.get("SamplingTime"));
            jsonData.setSamplingConclusion((String)jsonObject.get("SamplingConclusion"));
            jsonData.setPackingForm((String)jsonObject.get("PackingForm"));
            jsonData.setQuantityFilling((String)jsonObject.get("PackingForm"));
            jsonData.setFinalWeight((String)jsonObject.get("FinalWeight"));
            jsonData.setFillingLineNumber((String)jsonObject.get("FillingLineNumber"));
            jsonData.setMainExercise((String)jsonObject.get("MainExercise"));
            jsonData.setToReview((String)jsonObject.get("ToReview"));
            jsonData.setReportTime(getCalendar((String)jsonObject.get("ReportTime")).getTimeInMillis());
            JSONArray jsonArray = (JSONArray)jsonObject.get("OperationProcessList");
            ArrayList<OperationProcess> processArrayList = new ArrayList<OperationProcess>();
            for(int i=0;i<jsonArray.size();i++){
                OperationProcess operationProcess = new OperationProcess();
                operationProcess.setForignId(jsonData.getProductDate());
                operationProcess.setFKShowName((String)((JSONObject)jsonArray.get(i)).get("FKShowName"));
                operationProcess.setWLShowName((String)((JSONObject)jsonArray.get(i)).get("WLShowName"));
                operationProcess.setSetShowVal((String)((JSONObject)jsonArray.get(i)).get("SetShowVal"));
                operationProcess.setBeginTime(getCalendar((String)((JSONObject)jsonArray.get(i)).get("BeginTime")).getTimeInMillis()); //��Ҫת������
                operationProcess.setEndTime(getCalendar((String)((JSONObject)jsonArray.get(i)).get("EndTime")).getTimeInMillis());
                JSONArray fkcArray = (JSONArray)((JSONObject)jsonArray.get(i)).get("FKControlParameters");
                ArrayList<ControlParameter> cpList = new ArrayList<ControlParameter>();
                ControlParameter fkcCP ;
                for(int j=0;j<fkcArray.size();j++){
                    fkcCP = new ControlParameter();
                    fkcCP.setParmShowName((String)((JSONObject)fkcArray.get(j)).get("ParmShowName"));
                    fkcCP.setParmVal(Float.valueOf((String)((JSONObject)fkcArray.get(j)).get("ParmVal")));
                    fkcCP.setParmUD((String)((JSONObject)fkcArray.get(j)).get("ParmUD"));
                    fkcCP.setForignId(jsonData.getProductDate());
                    cpList.add(fkcCP);
                }
                operationProcess.setFKControlParameters(cpList);
                operationProcess.setEndState((String)((JSONObject)jsonArray.get(i)).get("EndState"));
                processArrayList.add(operationProcess);
            }
            jsonData.setOperationProcessList(processArrayList);
            System.out.println(jsonData.toString());
        }catch (Exception e){
            logger.error("returnData:data=>{},Exception=>{},msg=>{}",data,e.getMessage(),e);
            status = AnalysisStatusEnum.ANALYFAIL;

            jsonData = null;
            throw new RollbackException(e.toString());
        }
        status = AnalysisStatusEnum.ANALYSUCCESS;*/

        return jsonData;
    }


    public JsonData returnDataAuto(String data){
        JsonData jsonData = null;
        try {
            //转换数据
           jsonData = com.alibaba.fastjson.JSONObject.parseObject(data,JsonData.class);
           //设置关联关系
            for(ControlParameter controlParameter:jsonData.getControlParameters()){
                controlParameter.setForignId(jsonData.getReportTime());
            }
            for(OperationProcess operationProcess:jsonData.getOperationProcessList()){
                operationProcess.setForignId(jsonData.getReportTime());
            }
        }catch (Exception e){
            throw new InnerException(ErrorEnum.ANALYTICALDATAERROR.getCode(),e.getMessage(),e);
        }
        return jsonData;
    }



    public static void main(String[] args) {
       // AnalysisDataUtils analysisData = new AnalysisDataUtils();
       // analysisData.returnDataByPath("C:\\Users\\�ߺ�\\Desktop\\SN00016_20180822.json");
        Calendar calendar = new AnalysisDataUtils().getCalendar("2018-08-22");
        //long x = calendar.getTimeInMillis();
        System.out.println(calendar.getTimeInMillis());
        System.out.println(calendar.getTime().toString());
        Calendar calendar1 = Calendar.getInstance();

    }

    public Calendar getCalendar(String date){
        Calendar calendar = Calendar.getInstance();
        String[] strs = date.split(" ");
        if(strs.length==1){
            String[] dates = strs[0].split("-");
            calendar.set(Integer.valueOf(dates[0]),Integer.valueOf(dates[1])-1,Integer.valueOf(dates[2]),0,0,0);
        }else if(strs.length==2){
            String[] dates = strs[0].split("-");
            String[] mins = strs[1].split(":");
            calendar.set(Integer.valueOf(dates[0]),Integer.valueOf(dates[1])-1,Integer.valueOf(dates[2]),Integer.valueOf(mins[0]),Integer.valueOf(mins[1]),Integer.valueOf(mins[2]));
        }else{
            //�������ݸ�ʽ������
        }
        return calendar;
    }
}

//进入页面请求数据
$(document).ready(function () {
    //请求基础数据
    //var id = $('#notifyId').val();
    //if(id != "") {
    //    $.ajax({
    //        type: "post",
    //        url: "/notify/loanInfo",
    //        dataType: "json",
    //        data: {"loanProductId": id},
    //        success: function (data) {
    //            var loanData = data.data.loanProductData;
    //
    //            //直接显示在input中的数据
    //            $('#productName').val(loanData.productName);
    //
    //            $('#productIcon').val(loanData.productIcon);
    //
    //            $('#productIconImage').val(loanData.productIconImage);
    //            $('#productDesc').val(loanData.productDesc);
    //            $('#loanRate').val(loanData.loanRate);
    //            $('#applyUrl').val(loanData.applyUrl);
    //
    //            $('#applyCondition').val(loanData.applyCondition);
    //            $('#costDesc').val(loanData.costDesc);
    //            $('#repaymentDesc0').val(loanData.repaymentDesc[0]);
    //            $('#repaymentDesc1').val(loanData.repaymentDesc[1]);
    //            $('#repaymentDesc2').val(loanData.repaymentDesc[2]);
    //
    //            //单选
    //            $('#imageColor').val(loanData.imageColor).trigger('change');
    //            $('#loanRateType').val(loanData.loanRateType).trigger('change');
    //            $('#status').val(loanData.status).trigger('change');
    //            $('#needRealName').val(loanData.needRealName).trigger('change');
    //            $('#postUserInfo').val(loanData.postUserInfo).trigger('change');
    //            $('#profitType').val(loanData.profitType).trigger('change');
    //
    //            //多选
    //            if(loanData.applyMaterials != null) {
    //                var applyMaterials = loanData.applyMaterials.split(",");
    //                $('#applyMaterials').val(applyMaterials).trigger("change");
    //            }
    //            if(loanData.productTags != null) {
    //                var productTags = loanData.productTags.split(",");
    //                $('#productTags').val(productTags).trigger("change");
    //            }
    //            if(loanData.productDescType != null) {
    //                var productDescType = loanData.productDescType.split(",");
    //                $('#productDescType').val(productDescType).trigger("change");
    //            }
    //
    //            //简单数据处理
    //            if(loanData.loanPeriod != null) {
    //                var loanPeriod = loanData.loanPeriod.split(",");
    //                if(loanPeriod[0] != null) {
    //                    $('#loanPeriod0').val(loanPeriod[0]);
    //                }
    //                if(loanPeriod[1] != null) {
    //                    $('#loanPeriod1').val(loanPeriod[1]);
    //                }
    //            }
    //            if(loanData.age != null) {
    //                var age = loanData.age.split(",");
    //                if(age[0] != null) {
    //                    $('#age0').val(age[0]);
    //                }
    //                if(age[1] != null) {
    //                    $('#age1').val(age[1]);
    //                }
    //            }
    //            if(loanData.creditPoint != null) {
    //                var creditPoint = loanData.creditPoint.split(",");
    //                if(creditPoint[0] != null) {
    //                    $('#creditPoint0').val(creditPoint[0]);
    //                }
    //                if(creditPoint[1] != null) {
    //                    $('#creditPoint1').val(creditPoint[1]);
    //                }
    //            }
    //
    //            //复杂数据处理
    //            if(loanData.loanLimit != null) {
    //                var loanLimit = loanData.loanLimit.split(",");
    //                if(loanLimit[0] != null) {
    //                    if(loanLimit[0] / 10000 > 0 && loanLimit[0] % 10000 == 0) {
    //                        var data = loanLimit[0] / 10000 + "w";
    //                        $('#loanLimit0').val(data);
    //                    } else {
    //                        $('#loanLimit0').val(loanLimit[0]);
    //                    }
    //
    //                }
    //                if(loanLimit[1] != null) {
    //                    if(loanLimit[1] / 10000 > 0 && loanLimit[1] % 10000 == 0) {
    //                        var data = loanLimit[1] / 10000 + "w";
    //                        $('#loanLimit1').val(data);
    //                    } else {
    //                        $('#loanLimit1').val(loanLimit[1]);
    //                    }
    //                }
    //            }
    //        }
    //    });
    //}
    //select2初始化
    //$('#imageColor').select2();
    //$('#loanRateType').select2();
    //$('#status').select2();
    //$('#needRealName').select2();
    //$('#postUserInfo').select2();
    //$('#profitType').select2();
    //$('#applyMaterials').select2();
    //$('#productTags').select2();
    //$('#productDescType').select2();
    //动态改变select2样式
    $('.select2-container').css('width','200px');
});

//点击修改时向后台发送ajax请求
$('#modifyDetailBtn').click(function () {

    var id = $('#notifyId').val();
    var url;
    if((/^(\+|-)?\d+$/.test(id)) && id > 0){
        //拼接跳转
        url = "/spica/notify/notifyModify";
    } else if(id == "") {
        //若为空则刷新页面
        url = "/spica/notify/notifyAdd";
    }

    $.ajax({
        type: "post",
        url: url,
        dataType: "json",
        data: {
            //基本数据
            "id": $('#notifyId').val(),
            "hiveSql": $('#hiveSql').val(),
            "subject": $('#subject').val(),
            "notifyUser": $('#notifyUser').val(),
            "cronExpre": $('#cronExpre').val(),
            "status": $('#status').val(),
            "owner": $('#owner').val(),
        },
        success: function (data) {
            if (data.code == 100) {
                $('#modifyDetailBtn').attr("disabled", "disabled");
                alert("成功了,撒花~~~");
            } else {
                alert("请重新填写空值的选项");
            }
        }
    });

});

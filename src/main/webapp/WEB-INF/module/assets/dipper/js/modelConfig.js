//点击修改时向后台发送ajax请求
$('#updateModelConfig').click(function () {

    $.ajax({
        type: "post",
        url: "/spica/dipper-admin/updateModelConfig",
        dataType: "json",
        traditional: true,
        data: {

            "modelCode": $.map($("input[name='modelCode']"), function (obj) {
                return $(obj).val();
            }),
            "fullRuleType": $.map($("select[name='fullRuleType']"), function (obj) {
                return $(obj).val();
            }),
            "fullScriptName": $.map($("input[name='fullScriptName']"), function (obj) {
                return $(obj).val();
            }),
            "fullCron": $.map($("input[name='fullCron']"), function (obj) {
                return $(obj).val();
            }),
            "fullHiveQL": $.map($("input[name='fullHiveQL']"), function (obj) {
                return $(obj).val();
            }),
            "fullDriverType": $.map($("select[name='fullDriverType']"), function (obj) {
                return $(obj).val();
            }),
            "fullLoaderType": $.map($("select[name='fullLoaderType']"), function (obj) {
                return $(obj).val();
            }),
            "speedRuleType": $.map($("select[name='speedRuleType']"), function (obj) {
                return $(obj).val();
            }),
            "speedScriptName": $.map($("input[name='speedScriptName']"), function (obj) {
                return $(obj).val();
            }),
            "speedTopic": $.map($("input[name='speedTopic']"), function (obj) {
                return $(obj).val();
            }),
            "speedGroup": $.map($("input[name='speedGroup']"), function (obj) {
                return $(obj).val();
            }),
            "speedDriverType": $.map($("select[name='speedDriverType']"), function (obj) {
                return $(obj).val();
            }),
            "storeRuleType":$.map($("select[name='storeRuleType']"), function (obj) {
                return $(obj).val();
            }),
            "groovyScriptName":$.map($("input[name='groovyScriptName']"), function (obj) {
                return $(obj).val();
            }),
            "groovyConfig":$.map(window.editors, function (obj) {
                return obj.getValue();
            })

        },
        success: function (data) {
            if (data.code == 1001) {
                alert("修改成功");
            } else {
                alert("请重新填写空值的选项");
            }
        }
    });

});

function saveFullConfig(){
    var fullConf = $('#fullConfigHtml').clone();
    fullConf.find("select[name='addFullRuleType']").attr("name", "fullRuleType");
    fullConf.find("input[name='addFullScriptName']").attr("name", "fullScriptName");
    fullConf.find("input[name='addFullCron']").attr("name", "fullCron");
    fullConf.find("input[name='addFullHiveQL']").attr("name", "fullHiveQL");
    fullConf.find("select[name='addFullDriverType']").attr("name", "fullDriverType");
    fullConf.find("select[name='addFullLoaderType']").attr("name", "fullLoaderType");
    fullConf.attr("id", "");

    $('#fullConfigAddArea').append(fullConf);
    $('#fullConfigAddArea').append("<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\"\n" +
        "                                                    onclick=\"removeFullConfig(this)\">删除</button>");
    $('#addFullConfig').removeClass("show");
}

function saveSpeedConfig() {
    var speedConf = $('#speedConfigHtml').clone();
    speedConf.find("select[name='addSpeedRuleType']").attr("name", "speedRuleType");
    speedConf.find("input[name='addSpeedScriptName']").attr("name", "speedScriptName");
    speedConf.find("input[name='addSpeedTopic']").attr("name", "speedTopic");
    speedConf.find("input[name='addSpeedGroup']").attr("name", "speedGroup");
    speedConf.find("select[name='addSpeedDriverType']").attr("name", "speedDriverType");
    speedConf.attr("id", "");

    $('#speedConfigAddArea').append(speedConf);
    $('#speedConfigAddArea').append("<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\"\n" +
        "                                                        onclick=\"removeSpeedConfig(this)\">删除</button>");
    $('#addSpeedConfig').removeClass("show");
}

function saveGroovyConfig() {
    var scriptName = $('#groovyConfigHtml').find('input[name="addGroovyScriptName"]').val();
    var scriptContent = $('#groovyConfigHtml').find('textarea[name="addGroovyConfig"]').val();
    var innerHtml = "<div class=\"modal-body\">\n" +
        "                                                    <div><label>" + scriptName +"</label>\n" +
        "                                                        <input hidden name=\"groovyScriptName\"\n" +
        "                                                               value=\"" + scriptName +"\"/>\n" +
        "                                                        <textarea class=\"form-control codeMirror\" name=\"groovyConfig\" rows=\"10\">" + scriptContent + "</textarea>" +
        "                                                        <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\"\n" +
        "                                                            onclick=\"removeGroovyConfig(this)\">删除</button>" +
        "                                                    </div>\n" +
        "            </div>";
    var document = $(innerHtml);
    $('#groovyConfigAddArea').append(document);
    doBindCodeMirror(document.find('.codeMirror')[0]);
    $('#addGroovyConfig').removeClass("show");
}

function doBindCodeMirror(document){
    var editors = window.editors ? window.editors : [];
    var editor = CodeMirror.fromTextArea(document, {
        lineNumbers: true,
        matchBrackets: true,
        mode: "text/x-groovy"
    });
    editors.push(editor);
    window.editors = editors;
}

function bindCodeMirror(classname){
    var x = $("." + classname);
    for (var i = 0, j = x.length; i < j; i ++){
        doBindCodeMirror(x[i]);
    }
}

function addModelCode() {
    $.ajax({
        type: "post",
        url: "/spica/dipper-admin/addModelConfig",
        dataType: "json",
        traditional: true,
        data: {
            "modelCode":$('input[name="modelCode"]').val(),
            "modelDesc":$('input[name="modelDesc"]').val(),
            "modelName":$('input[name="modelName"]').val()
        },
        success: function (data) {
            if (data.code == 1001) {
                alert("修改成功");
            } else {
                alert("请重新填写空值的选项");
            }
        }
    });

    $('#addModelConfig').removeClass("show");
}

function syncFeatureConfigFromDMP(modelCode) {
    $.ajax({
        type: "post",
        url: "/spica/dipper-admin/syncFeatureConfigFromDMP",
        dataType: "json",
        traditional: true,
        data: {
            "modelCode":modelCode
        },
        success: function (data) {
            if (data.code == 1001) {
                alert("同步成功");
            } else {
                alert("同步失败");
            }
        }
    });
}

function startSourceJob(modelCode) {
    $.ajax({
        type: "post",
        url: "/spica/dipper-admin/startSourceJob",
        dataType: "json",
        traditional: true,
        data: {
            "modelCode":modelCode
        },
        success: function (data) {
            if (data.code == 1001) {
                alert("启动成功");
            } else {
                alert("启动失败");
            }
        }
    });
}

function removeFullConfig(obj) {
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}

function removeSpeedConfig(obj) {
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}

function removeGroovyConfig(obj) {
    obj.parentNode.parentNode.removeChild(obj.parentNode);
}
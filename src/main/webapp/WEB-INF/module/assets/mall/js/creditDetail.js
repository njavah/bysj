//进入页面请求数据
$(document).ready(function () {
    //请求基础数据
    var id = $('#id').val();
    if(id != "") {
        $.ajax({
            type: "post",
            url: "/spica/mall/creditInfo",
            dataType: "json",
            data: {"id": id},
            success: function (data) {
                var creditData = data.data.creditCardInfo;

                //直接显示在input中的数据
                $('#cardName').val(creditData.cardName);
                $('#themeDesc').val(creditData.themeDesc);
                $('#cardDesc').val(creditData.cardDesc);
                $('#cardSubDesc').val(creditData.cardSubDesc);
                $('#cardImgUrl').val(creditData.cardImgUrl);
                $('#cardTips').val(creditData.cardTips);
                $('#link').val(creditData.link);
                $('#relateAppId').val(creditData.relateAppId);
                $('#minAge').val(creditData.minAge);
                $('#maxAge').val(creditData.maxAge);

                //单选
                $('#isOutLink').val(creditData.isOutLink).trigger('change');
                $('#cardStyle').val(creditData.cardStyle).trigger('change');
                $('#cardColor').val(creditData.cardColor).trigger('change');
                $('#cardLevel').val(creditData.cardLevel).trigger('change');
                $('#cardCost').val(creditData.cardCost).trigger('change');
                $('#currency').val(creditData.currency).trigger('change');
                $('#status').val(creditData.status).trigger('change');

                //多选
                if(creditData.cardFunction != null) {
                    var cardFunction = creditData.cardFunction.split(",");
                    $('#cardFunction').val(cardFunction).trigger("change");
                }
                if(creditData.rightType != null) {
                    var rightType = creditData.rightType.split(",");
                    $('#rightType').val(rightType).trigger("change");
                }
                if(creditData.cardSystem != null) {
                    var cardSystem = creditData.cardSystem.split(",");
                    $('#cardSystem').val(cardSystem).trigger("change");
                }
            }
        });
    }
    //select2初始化
    $('#isOutLink').select2();
    $('#cardStyle').select2();
    $('#cardColor').select2();
    $('#cardLevel').select2();
    $('#cardCost').select2();
    $('#currency').select2();
    $('#status').select2();
    $('#cardFunction').select2();
    $('#rightType').select2();
    $('#cardSystem').select2();
    //动态改变select2样式
    $('.select2-container').css('width','200px');
});

//点击修改时向后台发送ajax请求
$('#modifyDetailBtn').click(function () {

    //处理多选数据
    var cardFunction = $('#cardFunction').val();
    var cardFunctionStr = "";
    if (cardFunction != null) {
        for (var i = 0; i < cardFunction.length; i++) {
            if (i != cardFunction.length - 1) {
                cardFunctionStr = cardFunctionStr + cardFunction[i] + ",";
            } else {
                cardFunctionStr = cardFunctionStr + cardFunction[i]
            }
        }
    }
    var rightType = $('#rightType').val();
    var rightTypeStr = "";
    if (rightType != null) {
        for (var i = 0; i < rightType.length; i++) {
            if (i != rightType.length - 1) {
                rightTypeStr = rightTypeStr + rightType[i] + ",";
            } else {
                rightTypeStr = rightTypeStr + rightType[i]
            }
        }
    }
    var cardSystem = $('#cardSystem').val();
    var cardSystemStr = "";
    if (cardSystem != null) {
        for (var i = 0; i < cardSystem.length; i++) {
            if (i != cardSystem.length - 1) {
                cardSystemStr = cardSystemStr + cardSystem[i] + ",";
            } else {
                cardSystemStr = cardSystemStr + cardSystem[i]
            }
        }
    }

    $.ajax({
        type: "post",
        url: "/spica/mall/creditModify",
        dataType: "json",
        data: {

            "id": $('#id').val(),
            "bankId": $('#bankId').val(),
            "cardName": $('#cardName').val(),
            "themeDesc": $('#themeDesc').val(),
            "cardDesc": $('#cardDesc').val(),
            "cardSubDesc": $('#cardSubDesc').val(),
            "cardImgUrl": $('#cardImgUrl').val(),
            "cardTips": $('#cardTips').val(),
            "link": $('#link').val(),
            "relateAppId": $('#relateAppId').val(),
            "minAge": $('#minAge').val(),
            "maxAge": $('#maxAge').val(),
            "isOutLink": $('#isOutLink').val(),
            "cardStyle": $('#cardStyle').val(),
            "cardColor": $('#cardColor').val(),
            "cardLevel": $('#cardLevel').val(),
            "cardCost": $('#cardCost').val(),
            "currency": $('#currency').val(),
            "status": $('#status').val(),

            //多选
            "cardSystemStr": cardSystemStr,
            "rightTypeStr": rightTypeStr,
            "cardFunctionStr": cardFunctionStr
        },
        success: function (data) {
            if (data.code == 100) {
                $('#modifyDetailBtn').attr("disabled", "disabled");
                alert("修改成功");
            } else {
                alert("请重新填写空值的选项");
            }
        }
    });

});

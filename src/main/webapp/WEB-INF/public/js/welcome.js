$(document).ready(function() {
    var table = $('#employees').dataTable( {        				
    	"oLanguage": {
    		"sProcessing":   "处理中...",
    	    "sLengthMenu":   "_MENU_ 记录/页",
    	    "sZeroRecords":  "没有匹配的记录",
    	    "sInfo":         "显示第 _START_ 至 _END_ 项记录，共 _TOTAL_ 项",
    	    "sInfoEmpty":    "显示第 0 至 0 项记录，共 0 项",
    	    "sInfoFiltered": "(由 _MAX_ 项记录过滤)",
    	    "sSearch":       "过滤:",
    	    "oPaginate": {
    	        "sFirst":    "首页",
    	        "sPrevious": "上页",
    	        "sNext":     "下页",
    	        "sLast":     "末页"
    	    }
    	},
	    "bAutoWidth": false,
		"aaSorting": [
		              	[0, "asc"]
		             ],
		"aoColumnDefs": [
	                     {"bSortable": false, "aTargets": []}, 
	                     {"sWidth":"10%", "aTargets":[0]},
	                     {"sWidth":"15%", "aTargets":[1]},
	                     {"sWidth":"10%", "aTargets":[2]},
	                     {"sWidth":"32%", "aTargets":[3,4]},
	                    ]
	                    
	});
    
    $("#liWelcome").addClass("active");
});

var Employees = {
    loadData:function(data) {
	   var result = [];
	   var i = 1;
	   $.each(data, function(key, value) {
		   var tempObj = new Array();
		   tempObj.push(i++);
		   tempObj.push(value["name"]);
		   tempObj.push(value["sex"]);
		   tempObj.push(value["mobile"]);
		   tempObj.push(value["email"]);
		   result.push(tempObj);
	   });
	   
	   return result;
   	},
   	
   	loadAllData: function() {
   		$.get(contextPath + "/employee/list", {}, function(response) {
   			var result = JSON.parse(response);
   			if (result.code == 1001 && result.data.length > 0) {
   				var data = result.data;
   				$('#employees').dataTable().fnClearTable();
   				$('#employees').dataTable().fnAddData(Employees.loadData(data), true);
   			} else {
   				$('#employees').dataTable().fnClearTable();
   			}
   		});
   	}
}

//加载所有数据
Employees.loadAllData();
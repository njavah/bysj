/**
 * Created by chenxi on 01/12/2016.
 */

$(document).ready(function () {
//daterangepicker 基本配置及操作
    $('.J_datarange').each(function (i, item) {
        (function ($daterange) {
            $daterange.daterangepicker({
                // dateLimit: {
                //     "months": 2
                // },
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: $daterange.find('.J_starttime').val() || undefined,
                endDate: $daterange.find('.J_endtime').val() || undefined,

            });

            if ($daterange.find('.J_starttime').val() != '' && $daterange.find('.J_endtime').val() != '') {
                $daterange.find('input[type=text]').val($daterange.find('.J_starttime').val() + ' 至 ' +$daterange.find('.J_endtime').val());
            }

            $daterange.on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate,
                    end = picker.endDate;
                $daterange.find('input').val(start.format('YYYY-MM-DD') + ' 至 ' + end.format('YYYY-MM-DD'));
                $daterange.find('.J_starttime').val(start.format('YYYY-MM-DD'));
                $daterange.find('.J_endtime').val(end.format('YYYY-MM-DD'));
            });
            //清空选择
            $daterange.on('cancel.daterangepicker', function (ev, picker) {
                $daterange.find('input').val("");
                $daterange.find('.J_starttime').val("");
                $daterange.find('.J_endtime').val("");
            });

        }($(item)));
    });

    $('.J_datepicker').each(function () {
        $(this).datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true,

        });

        // var dateStr = $(this).find('.J_dateStr').val();
        //
        // if ('' != dateStr) {
        //     $(this).find('.J_dateTimestamp').val(moment(dateStr).format('X'));
        // }

        $(this).on('show', function (e) {
            // $(this).find('input').val("");
            $(this).datepicker('update', '');
        });

        $(this).on('hide', function (e) {
            console.log(e);
            console.log(this);

            var dateStr = $(this).find('.J_dateStr').val();

            if ("" === dateStr) {
                $(this).find('.J_dateTimestamp').val('');
            } else {
                $(this).find('.J_dateTimestamp').val(moment(dateStr).format('X'));
            }
        });

        $(this).on('clearDate', function (ev, picker) {
            $(this).find('input').val("");
        })
    });

});

//插件初始化
$(".select2").select2();
$('.J_switch').bootstrapSwitch();


$(".jsNavBar").removeClass("active");




//formReset
function formReset(formId) {
    $(formId).find('.select2').val("").trigger('change');
    $(formId).find('input').val("");
    $(formId).find('.J_starttime').val("");
    $(formId).find('.J_endtime').val("");
    $(formId).find('.J_dateStr').val("");
    $(formId).find('.J_dateTimestamp').val("");

    // $('.J_datepicker').each(function () {
    //     $(this).datepicker({
    //         format: 'yyyy-mm-dd',
    //         autoclose: true,
    //         clearBtn: true,
    //
    //     });
    //
    //     var dateStr = $(this).find('.J_dateStr').val();
    //
    //     if ('' != dateStr) {
    //         $(this).find('.J_dateTimestamp').val(moment(dateStr).format('X'));
    //     }
    //
    //
    //     $(this).on('hide', function (e) {
    //         console.log(e);
    //         console.log(this);
    //
    //         var dateStr = $(this).find('.J_dateStr').val();
    //
    //         if ("" === dateStr) {
    //             $(this).find('.J_dateTimestamp').val('');
    //         } else {
    //             $(this).find('.J_dateTimestamp').val(moment(dateStr).format('X'));
    //         }
    //     });
    //
    //     $(this).on('clearDate', function (ev, picker) {
    //         $(this).find('input').val("");
    //     })
    // });
}

var opts = {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-bottom-right",
    "onclick": null,
    "showDuration": "300",
    // "hideDuration": "3000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

function opSuccess() {
    toastr.success("", "操作成功！", opts);
}

function opFailed(msg) {
    toastr.error(msg, "操作失败！", opts);
}

function opWarning(msg) {
    toastr.warning(msg, "操作警告！", opts);
}

function opInfo(msg) {
    toastr.info(msg, "提示！", opts);
}

//判断是否有权限
function checkRight(checkUrl) {
    $.ajax({
        url: checkUrl,
        type: 'POST',
        data: null,
        dataType: 'html',
        async: true
    }).done(function (ret) {
        console.log('ret：' + ret);

        if (!hasRight(ret)) {// 有权限
            window.onbeforeunload = null;
            window.location.href = checkUrl;
            return;

        }
    });
}

//判断是否有权限
function hasRight(response) {
    if (response != null && response != '' && response != 'undefine') {
        if (response.indexOf("auth.meili-inc.com") == -1
            && response.indexOf("auth.mogujie.org") == -1) {// 不包含这个字眼则说明有权限
            return true;
        }
        return false;
    }
    return true;
}

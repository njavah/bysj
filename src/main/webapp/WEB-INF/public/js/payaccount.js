function Delete() {
	var userId = "";
    $("#dataTable").find("tbody").find("tr").each(function(i) { 
    	var checked = 0;
    	$(this).find("input[type=checkbox]").each(function(i) {
    		if($(this).prop('checked') == true) {
    			checked = 1;
    		}
    	})
    	if(checked == 1) {
    		var _userId = $(this).find('td').eq(1).text()
    		userId += _userId + ";";
    	}
    }) 
    if(userId == "") {
    	alert("please choose at least one item！");
    } else {
	    if (confirm("are you sure to delete？")) {  
	    	window.location.href='delete?deleteUserIds=' + userId;
        }  
    }
}

function Edit() {
	var userId = "";
    $("#dataTable").find("tbody").find("tr").each(function(i) { 
    	var checked = 0;
    	$(this).find("input[type=checkbox]").each(function(i) {
    		if($(this).prop('checked') == true) {
    			checked = 1;
    		}
    	})
    	if(checked == 1) {
    		var _userId = $(this).find('td').eq(1).text()
    		userId += _userId + ";";
    	}
    }) 
    if(userId == "") {
    	alert("please choose one item！");
    } else if(userId.split(";").length > 2){
	    alert("more than one item was selected"); 
    } else {
    	var editUserId = userId.split(";")[0];
	    window.location.href='edit?editUserId=' + editUserId + '&t=1';
    }
}

function SelectAll() {
	var checked = false;
	 $("#dataTable").find("thead").find("input[type=checkbox]").each(function(i) { 
		 if($(this).prop('checked') == true) {
 			checked = true;
 	     } else {
 	    	checked = false;
 	     }
	 }) 
	 //全选|全不选
	 $("#dataTable").find("tbody").find("tr").each(function(i) { 
	    	$(this).find("input[type=checkbox]").each(function(i) {
	    		$(this).prop('checked', checked);
	    	})
	 }) 
}

$(document).ready(function() {
	$("#liPayAccountCfg").addClass("active");
});
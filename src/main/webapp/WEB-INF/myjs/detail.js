$(function () {


    $("#dataplatform").addClass("active");
    $("#navBar_Query_Information").addClass("active");
    $("#navBar_Query_Information").css({"color":"wheat"});


    var fileName = $("#fileName").val();
    if(fileName != ""){
        $("#controlParametersTable").dataTable({
            "autoWidth": true,
            "ajax": {
                "url": "/jsondata/listDetail",
                "data":{fileName:fileName},
                "dataSrc": function (data) {
                    console.log(data);
                    if(data.code == 1011 && data.controlParameterList ){
                        return data.controlParameterList;
                    }else{
                        return new Array();
                    }
                },
            },
            "deferRender": true,
            "lengthMenu": [15, 25, 50, 75, 100],
            //允许重建
            "destroy": true,
            "columns": [
                {
                    "data": "parmShowName",
                    "title":"ParmShowName"
                },
                {
                    "data": "parmVal",
                    "title":"ParmVal"
                },
                {
                    "data": "parmUD",
                    "title": "ParmUD"
                }
            ],
            /* "columnDefs": [{
                 targets: [4], //第1，2，3列
                 createdCell: function (td, cellData, rowData, row, col) {
                     //td:已经创建的td节点 row 行 col 列 rowdata 整行数据 cellData 单元格的数据
                     console.log(row+":"+col);
                     console.log(JSON.stringify(cellData));
                     /!*  cellData = rowData.productType1+"-"+rowData.type1Comment;
                       if(row ==0 && (col == 4 || col == 5)){
                           $(td).attr('rowspan',3);
                           //$(td).attr('colspan',1);
                       }else if(row == 4 && (col == 4 || col == 5)){
                           $(td).attr('rowspan',3);
                           //$(td).attr('colspan',1);
                       }else {
                           $(td).remove();
                       }*!/

                 }
             }],*/
            //设置排序
            "order": [[1, 'asc']]
        })


        $("#operationProcessTable").dataTable({
            "autoWidth": true,
            "ajax": {
                "url": "/jsondata/listDetail",
                "data":{fileName:fileName},
                "dataSrc": function (data) {
                    console.log(data);
                    if(data.code == 1011 && data.operationProcessList){
                        return data.operationProcessList;
                    }else{
                        return new Array();
                    }
                }
            },
            "deferRender": true,
            "lengthMenu": [15, 25, 50, 75, 100],
            //允许重建
            "destroy": true,
            "columns": [
                {
                    "data": "fKShowName",
                    "title":"FKShowName"
                },
                {
                    "data": "wLShowName",
                    "title":"WLShowName"
                },
                {
                    "data": "setShowVal",
                    "title": "SetShowVal"
                },
                {
                    "data":"beginTime",
                    "title":"BeginTime",
                    "render":function (data) {
                        return new Date(data).toLocaleString();
                    }
                },
                {
                    "data":"endTime",
                    "title":"EndTime",
                    "render":function (data) {
                        return new Date(data).toLocaleString();
                    }
                },
                {
                    "data":"endState",
                    "title":"EndState"
                },
                {
                    "data":"",
                    "title":"参数列表",
                    "render":function (data, type, row, meta) {
                        var url = "javascript:openModal("+row.id+")";
                        var urlStr = "<a href='"+url+"'>参数详情</a>";
                        return urlStr
                    }
                }
            ],
            /* "columnDefs": [{
                 targets: [4], //第1，2，3列
                 createdCell: function (td, cellData, rowData, row, col) {
                     //td:已经创建的td节点 row 行 col 列 rowdata 整行数据 cellData 单元格的数据
                     console.log(row+":"+col);
                     console.log(JSON.stringify(cellData));
                     /!*  cellData = rowData.productType1+"-"+rowData.type1Comment;
                       if(row ==0 && (col == 4 || col == 5)){
                           $(td).attr('rowspan',3);
                           //$(td).attr('colspan',1);
                       }else if(row == 4 && (col == 4 || col == 5)){
                           $(td).attr('rowspan',3);
                           //$(td).attr('colspan',1);
                       }else {
                           $(td).remove();
                       }*!/

                 }
             }],*/
            //设置排序
            "order": [[1, 'asc']]
        })
    }
})


function openModal(data){
    $("#myModal").modal('show');

    $("#operationTable").dataTable({
        "autoWidth": true,
        "ajax": {
            "url": "/jsondata/listOperationParameter",
            "data":{id:data},
            "dataSrc": function (data) {
                console.log(data);
                if(data.code == 1011 && data.data){
                    return data.data;
                }else{
                    return new Array();
                }
            }
        },
        "deferRender": true,
        "lengthMenu": [15, 25, 50, 75, 100],
        //允许重建
        "destroy": true,
        "columns": [
            {
                "data": "parmShowName",
                "title":"ParmShowName"
            },
            {
                "data": "parmVal",
                "title":"ParmVal"
            },
            {
                "data": "parmUD",
                "title": "ParmUD"
            }
        ],
        /* "columnDefs": [{
             targets: [4], //第1，2，3列
             createdCell: function (td, cellData, rowData, row, col) {
                 //td:已经创建的td节点 row 行 col 列 rowdata 整行数据 cellData 单元格的数据
                 console.log(row+":"+col);
                 console.log(JSON.stringify(cellData));
                 /!*  cellData = rowData.productType1+"-"+rowData.type1Comment;
                   if(row ==0 && (col == 4 || col == 5)){
                       $(td).attr('rowspan',3);
                       //$(td).attr('colspan',1);
                   }else if(row == 4 && (col == 4 || col == 5)){
                       $(td).attr('rowspan',3);
                       //$(td).attr('colspan',1);
                   }else {
                       $(td).remove();
                   }*!/

             }
         }],*/
        //设置排序
        "order": [[1, 'asc']]
    })
}


Date.prototype.toLocaleString = function() {
    var year = this.getFullYear();
    var month = this.getMonth()+1;
    month = month<10?'0'+month:month;
    var day = this.getDate();
    day = day<10?'0'+day:day;
    var hours = this.getHours();
    hours = hours<10?'0'+hours:hours;
    var min = this.getMinutes();
    min = min<10?'0'+min:min;
    var ss = this.getSeconds();
    ss = ss<10?'0'+ss:ss;
    return year+'-'+month+'-'+day+"   "+hours+":"+min+":"+ss;
    /*return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate() + "- " + this.getHours() + ":" + this.getMinutes() + ":" + this.getSeconds() ;*/
};
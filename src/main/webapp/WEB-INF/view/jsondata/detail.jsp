<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>数据管理平台</title>
    <!-- Tell the browser to be responsive to screen width -->


    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">

    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/startboot/font-awesome.min.css">

    <style type="text/css">
        a {
            text-decoration:none !important; /*CSS下划线效果：无下划线*/
        }
    </style>

    <link rel="stylesheet" href="/AdminLTE/dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="/AdminLTE/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="/AdminLTE/bootstrap/css/bootstrap-datetimepicker.css">



    <!-- Font Awesome -->


    <link rel="stylesheet" href="/AdminLTE/plugins/select2/select2.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/AdminLTE/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/AdminLTE/dist/css/skins/skin-purple.min.css">
    <link rel="stylesheet" href="/css/bootstrap3.css">


    <link rel="stylesheet" href="/AdminLTE/pcm/maintain.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.1/css/select.dataTables.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">




    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/AdminLTE/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/AdminLTE/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="/AdminLTE/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="/AdminLTE/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="/AdminLTE/plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css">

    <link rel="stylesheet" href="/growl/css/jquery.notify.css">
    <link rel="stylesheet" href="/switch/bootstrap-toggle.min.css">
    <script>
        var contextPath = "";
    </script>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper" style="background-color: #ecf0f5 !important;">
    <!-- Navbar-->
    <header class="main-header hidden-print"><a class="logo" href="/">数据管理平台</a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
            <!-- Navbar Right Menu-->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav top-nav">


                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle headerColor" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="/png/1.jpg" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">${sessionScope.userName}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/png/1.jpg" class="img-circle" alt="User Image">

                                <p>
                                    ${sessionScope.userName}
                                </p>
                            </li>
                            <li class="user-footer">

                                <div class="pull-right">
                                    <a href="/jsondata/exitLogin" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- 帮助手册 -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-question-circle-o fa-lg"></i></a>
                    </li>

                    <!-- 风格设置 -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gears fa-lg"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print">
        <section class="sidebar">



            <!-- Sidebar Menu-->
            <ul class="sidebar-menu">
                <li class="treeview" id="dataplatform"><a href="#"><i class="fa fa-databaser"></i><span>数据管理</span><i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu" >
                        <li ><a href="/jsondata/upload" id="navBar_Upload_Information"><i class="fa fa-cloud-upload"></i>上传数据</a></li>
                        <li ><a href="/jsondata/index" id="navBar_Query_Information"><i class="fa fa-search"></i>基础查询</a></li>
                    </ul>
                </li>

            </ul>
        </section>
    </aside>



    <div class="content-wrapper" style="background-color: #ecf0f5 !important;">

        <!-- 客户服务平台 -->
        <ul class="breadcrumb">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li class="active">基础数据</li>
            <li class="active">数据查询</li>
        </ul>
        <!-- 根据用户信息查询条件 -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">基础信息</h3>
                        </div>
                        <form class="form-horizontal" role="form" method="get" id="search">
                            <div class="box-body">
                                <div class="form-group">

                                    <label class="col-md-1 control-label">文件名</label>
                                    <div class="col-md-3">
                                        <input type="text" id="fileName" class="form-control"  placeholder="" value="${fileName}" disabled/>
                                    </div>


                                    <label class="col-md-1 control-label">批量编号</label>
                                    <div class="col-md-3">
                                        <input type="text" id="time" class="form-control"  placeholder="" value="${batchNum}" disabled/>
                                    </div>


                                    <label class="col-md-1 control-label">生产时间</label>
                                    <div class="col-md-3">
                                        <input type="text" id="batchNum" class="form-control"  placeholder="" value="${time}" disabled/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3>ControlParameters</h3>
                        </div>
                        <div class="box-body">
                            <p>
                            <table  class="table table-bordered table-striped " id="controlParametersTable">

                            </table>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3>OperationProcess</h3>
                        </div>
                        <div class="box-body">
                            <p>
                            <table  class="table table-bordered table-striped " id="operationProcessTable">

                            </table>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">生产参数</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <table  class="table table-bordered table-striped " id="operationTable">

                        </table>

                    </form>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>




</div>

<script src="/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>

<script src="/growl/js/jquery.notify.min.js"></script>

<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>



<script src="/js/jquery.serializeObject.min.js"></script>
<script src="/AdminLTE/plugins/daterangepicker/moment.min.js"></script>
<script src="/AdminLTE/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/AdminLTE/plugins/select2/select2.full.min.js"></script>
<script src="/AdminLTE/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="/AdminLTE/plugins/toastr/toastr.min.js"></script>
<script src="/switch/bootstrap-toggle.min.js"></script>
<script src="/js/payCommon.js"></script>
<script src="/js/bootbox.min.js"></script>


<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/main.js"></script>
<script src="/assets/js/pace.min.js"></script>
<script src="/AdminLTE/bootstrap/js/bootstrap.min.js"></script>

<script src="/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
<script src="/myjs/detail.js"></script>

<%--<script src="/js/upload.js"></script>--%>
</body>
</html>



